import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

let apiUrl = '/v1/';

@Injectable()
export class AuthService {
    public uid: string;
    public client: string;
    public access_token: string;
    public data: any;
    public isLoggedIn: boolean = false;
    results: any[] = [];

    public contentHeader = new Headers();

  constructor(
    public http: Http,
    private alertCtrl: AlertController,
    public storage: Storage) 
    {
    this.data = null;
    }

  getHeader(){
     return this.contentHeader;
  }

  saveHeader(token, uid, client){
    this.storage.set('token', token);
    this.storage.set('uid', uid);
    this.storage.set('client', client);
    this.contentHeader.append('Content-Type', 'application/json');
    this.contentHeader.append('access-token', token);
    this.contentHeader.append('client', client);
    this.contentHeader.append('uid', uid);
  };

  login(credentials) {
    let headers = new Headers();
    return new Promise((resolve, reject) => {
      headers.append('Content-Type', 'application/json');
      return this.http.post(apiUrl+'sign_in?', JSON.stringify(credentials), {headers: headers})
        .subscribe(res => 
        {

          if( res.status == 200){
              console.log('Status 200');
              this.saveHeader(
                  res.headers.get('access-token'),
                  res.headers.get('uid'),
                  res.headers.get('client'
              ));
          }  

          resolve(res.json());
        }, (err) => 
        {

          if (err.status == 401) {
            console.log('Status 401, error en login');
          }
          reject(err);
        });
    
    });
  }


  load() {
       if (this.data) {
          // already loaded data
          return Promise.resolve(this.data);
        }
        return new Promise((resolve, reject) => 
        {
        this.http.get(
          apiUrl+'championships/1/games/2017-03-28/results', { headers: this.getHeader()})
          .map(res => res.json())
            .subscribe(data => 
            {
              this.data = data.games;
              resolve(this.data);
            }, (err) => 
            {
                if (err.status == 401) {
                  console.log('Status 401, error en la autenticacion al cargar data');
                }
              reject(err);
            });
       });
  }

  logout(){
     this.storage.clear();
  }

}
