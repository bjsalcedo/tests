import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { LoginPage } from '../login/login';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root: any = HomePage;
  tab2Root: any = AboutPage;
  tab3Root: any = ContactPage;

  constructor(public navCtrl: NavController,
              public storage: Storage) {


 
       storage.get('token').then((val) => {

       console.log('tabs verificacion', val);

       if(val == null)
          {
            console.log('SetRoot LoginPage');
            navCtrl.setRoot(LoginPage);
          }
          else 
          {
            console.log('Home Page');
            }
        
       })
 

  }
}
