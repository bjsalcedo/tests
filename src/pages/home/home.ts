import { Component } from '@angular/core';
import { AuthService } from '../../providers/auth-service';
import { NavController, App, LoadingController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage { 

  public loading: any;
  public isLoggedIn: boolean = false;
  public results: any[] = [];
  public loginData = {
    email:'brayan.salcedo@condoragency.com', 
    password:'qweasd123@'
  };
  

  constructor(
    public app: App, 
    public storage: Storage,
    public navCtrl: NavController, 
    public authService: AuthService, 
    public loadingCtrl: LoadingController, 
    private toastCtrl: ToastController) {

    storage.ready().then(() => {
       storage.get('token').then((val) => {
       if(val != null)
          {
            console.log('usuario logeado y su token es ', val);
            this.isLoggedIn = true;
            this.doLogin();
            
          }
          else
          {
            console.log('No se encuentra loggeado');
          }
        
       })
     });
  }


  doLogin() {
    this.showLoader();
    this.authService.login(this.loginData).then((result) => {
      this.showResults();
        this.loading.dismiss();
    }, (err) => {
      this.loading.dismiss();
      this.presentToast(err);
    });
  }

  showResults(){ 
    this.authService.load()
      .then(res => {
          console.log(res);
          this.results = res;
      });
  }

  logout() {
      this.authService.logout();
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Obteniendo resultados...'
    });

    this.loading.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

}
