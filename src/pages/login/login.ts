import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { TabsPage } from '../tabs/tabs';
import { RegisterPage } from '../register/register';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  loading: any;
  loginData = {email:'brayan.salcedo@condoragency.com', password:'qweasd123@'};
  data: any;
  constructor(public navCtrl: NavController, 
    public authService: AuthService, 
    public loadingCtrl: LoadingController, 
    public storage: Storage,
    private toastCtrl: ToastController) 
  {
    
  }
  doLogin() {
    this.showLoader();
    this.authService.login(this.loginData).then((result) => {
      this.loading.dismiss();
        this.navCtrl.setRoot(TabsPage);
    }, (err) => {
      this.loading.dismiss();
      this.presentToast(err);
    });
  }
  register() {
    this.navCtrl.push(RegisterPage);
  }
  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Autenticando...'
    });
    this.loading.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }

}
